# Window-Dumps for explaining the Basics

Yes, window-dumps, not screenshots.
You don't need to take screenshots of the current window of `plio`,
you can just press the `d` key and a window-dump will be saved to a png file.
This works in Dirs--View, Index-View and Image-View.

Let's now together look at how `plio` looks like and how to handle it.
It's assumed here,
that you already can start `plio` with directory arguments from the command line.

## Dirs-View
Initially all accepted directories (at least one file with image-file-extension must be contained)
will be represented by a thumbnail (of the first image of a directory in name sorted order),
and you see the thumbs of the directories that have been accepted by `plio` presented as a 2D-array like this:

![sirsview buttersafe](pics/dirsview_buttersafe.png "dirsview - focussed buttersafe")

The <ins>number of directories</ins> is shown above the thumbnail array.
The green rectangle is the **cursor**.

The <ins>name of the directory</ins>, which is highlighted by the cursor,
as well as the <ins>number of files</ins> (or if the directory has been read, the number of readable images)
that are contained in that directory,
are displayed below the thumbnail array.

You can move the cursor with the cursor keys or with vim-like keys to other directories:

![dirsview oglaf](pics/dirsview_oglaf.png "dirsview - focussed oglaf")

moved to the last position:

![dirsview xkcd](pics/dirsview_xkcd.png "dirsview - focussed xkcd")



## Index-View

We moved the cursor back to the first position (index 0) and pressed `Enter`.
As soon as the `Enter` key has been pressed,
`plio` enters the directory that was highlighted with the cursor.


The images of that directory will then be read and their thumbnails presented:

![indexview of buttersafe-comics, 14 of 21 images read already](pics/indexview_buttersafe_14_images_read.png "14 of 21 images read")

As you can see,
14 images out of 21 files have aleady been read at the time the window-dump has been created.
When all images could be read and decompressed,
the number of images equals the number of files,
as you can be seen in the next window dump.

![indexview of buttersafe-comics, all 21 images have been read](pics/indexview_buttersafe_21_images_read_completed.png "all 21 image files read")

If there were files with image file extension,
which could not be read and decompressed as an image,
the number of images would be less than the number of files.

In the Index-View (which shows the images of one directory as thumbnails)
you can move around with the same keys as were used in the Dirs-View.

Here we moved to position 11:
![indexview buttersafe-comics at pos.idx 11](pics/indexview_buttersafe_viewindex_11.png "indexview of buttersafe-dir pos.idx 11")


With the `q` key you can go back upwards to the Dirs-View,
select another directory and then look inside it:

![indexview of pbf-comids, index 0](pics/indexview_pbf_indexview_00.png "indexview of pbf, pos.idx 0")


Pressing `Enter` while you are in the Index-View would open the image under the cursor (Image-View).
Pressing `Enter` again would lead you back to the Index-View.

With the `q` key you would go back upwards to the Dirs-View (from the Index-View as well as from the Image-View).


## Index-View, with images sorted by ...

We now show some of the sorting possibilities.

We are again in the first directory and have set the cursor back at position 0.
(The position of the cursor does not matter for sorting; it has been mentioned here,
because when you leave a directory, `plio` remembers,
at which position the cursor has been,
when visited the last time.
This may be confusing at the beginning, but quite convenient, when you have been accustomed to it.)

The default sorting is sort-by-name:
![sorted by name](pics/01_sorted_by_name.png "sorted by name (default)")

You can sort by width with `Ctrl-w`:
![sorted by width](pics/02_sorted_by_width.png "sorted by width")

You can sort by height with `Ctrl-h`:
![sorted by height](pics/03_sorted_by_height.png "sorted by height")

You can sort by size (width x height) with `Ctrl-s`:
![sorted by size](pics/04_sorted_by_size.png "sorted by size")

If you wish to sort by aspect ratio, use `Ctrl-a`:
![sorted by aspect ratio](pics/05_sorted_by_aspectratio.png "sorted by aspect ratio")


When you want to go back to sort-by-name press `Ctrl-n`:

![image shows selected Dir sorted by name](pics/01_sorted_by_name.png "sorted by name (reestablished)")


## Image-View
As mentioned above,
you can go to the Image-View,
when pressing `Enter` while you are in the Index-View.
As the name suggests, the Image-View shows you an image - the one that the cursor has highlighted.

Pressing `Enter` again brings you back to the Index-View.
Pressing `q`  brings you up to the Dirs-View.

Currently (2023-05-12), the Image-View of `plio` only supports
"fit to window". Scaling the image might be added later.

You can use the same keys that were used to navigate in the Dirs-View and the Index-View
to also navigate in the Image-View.
But by doing so you don't navigate inside the image - you navigate in the Index (that from the Index-View)
while you look at images in the Image-View.
That means you can move to the next image to the left or the right while looking at the images.
But you can also move to the next image below or above.

This allows skimming through the images without the need to go
to the Index-View first (with the `Enter` key),
then move the cursor in the Index-View,
and the  open an image (again with the `Enter` key) in the Image-View.

Instead you just move to the adjacent image (left, right, above, below)
directly.
(Of course,
when you want to select not the next adjacent image but an arbitrarily image,
you need to go to the Index-View and select the image there.)

Moving to the adjacent left or right image is,
what you surely know from other image viewers.

But moving vertical through the image array allows you to skim fast
through a lot of images - similar to taking samples.
If the `plio` window shows an image array of say 10 x 15 images,
moving vertical would show you every 10th image.

# Reversing the Sorting-Order (Ascending vs. Descending)
All of the above mentioned sorting functionality does sort in ascending order.
To get descending order, first sort as mentioned above,
then use `Ctrl-r` to reverse the order of the arrays.

This reversing is implemented for Dirs-View as well as Index-View.
